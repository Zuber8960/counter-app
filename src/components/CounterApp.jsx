import React, { useState } from "react";
import Item from "./Item";
import Reset from "./Reset";

const CounterApp = () => {

  const products = [
    {
        id:1,
        name: "product1",
        quantity: "Zero",
    },
    {
        id:2,
        name: "product2",
        quantity: "Zero"
    },
    {
        id:3,
        name: "product3",
        quantity: "Zero"
    },
    {
        id:4,
        name: "product4",
        quantity: "Zero"
    }
  ];

  const [prods, setProds] = useState(products);
  const [arr,setArr] = useState([]);

//   console.log(prods);
  return (
    <div className="container">
      <div className="top-container">
        <i className="fa-solid fa-cart-shopping"></i>
        <span className="quantity">{arr.length}</span>
        <span>Items</span>
      </div>
      <Reset products={products} prods={prods} setProds={setProds} setArr={setArr}/>

      <div className="items">
        {prods.map((product) => {
            return <Item key={product.id} id={product.id} quantity={product.quantity} prods={prods} setProds={setProds} arr={arr} setArr={setArr}/>
        })}
      </div>
    </div>
  );
};

export default CounterApp;
