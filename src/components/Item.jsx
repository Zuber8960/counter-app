import React, { useState } from "react";

const Item = ({ id, quantity, prods, setProds, arr, setArr }) => {
  function increament(event) {
    // const result = prods.map(({ ...product }) => {
    //   if (product.id === id) {
    //     if (product.quantity === "Zero") {
    //       product.quantity = 1;
    //       arr.push(id);
    //       setArr(arr);
    //     } else {
    //       product.quantity++;
    //     }
    //   }
    //   return product;
    // });

    // setProds(result);

    setProds((oldsProducts) => {
      return oldsProducts.map((oldsProduct) => {
        if (oldsProduct.id === id) {
          if (oldsProduct.quantity === "Zero") {
            oldsProduct.quantity = 1;
            setArr([...arr, id]);
          } else {
            oldsProduct.quantity++;
          }
        }
        return oldsProduct;
      });
    });
  }

  function decreament(event) {
    // const result = prods.map(({ ...product }) => {
    //   if (product.id === id) {
    //     if (product.quantity === "Zero" || product.quantity === 1) {
    //       if (product.quantity === 1) {
    //         const newArr = arr.filter((productId) => {
    //           return productId !== id;
    //         });
    //         setArr(newArr);
    //       }
    //       product.quantity = "Zero";
    //     } else {
    //       product.quantity--;
    //     }
    //   }
    //   return product;
    // });
    // setProds(result);

    setProds((oldProducts) => {
      return oldProducts.map((oldProduct) => {
        if (oldProduct.id === id) {
          if (oldProduct.quantity === "Zero" || oldProduct.quantity === 1) {
            if (oldProduct.quantity === 1) {
              setArr((oldArr) => {
                return oldArr.filter((productId) => productId != id);
              });
            }
            oldProduct.quantity = "Zero";
          } else {
            oldProduct.quantity--;
          }
        }
        return oldProduct;
      });
    });
  }

  function deleteList(event) {
    // const newArr = arr.filter((productId) => {
    //   return productId !== id;
    // });
    // setArr(newArr);
    // const newProducts = prods.filter(({ ...product }) => {
    //   return product.id !== id;
    // });

    // setProds(newProducts);

    setProds(oldProducts => {
      return oldProducts.filter(oldProduct => oldProduct.id !== id);
    });

    setArr(productIds => {
      return productIds.filter(productId => productId !== id);
    })
  }

  return (
    <div className="counter">
      <span
        className={
          (typeof quantity === "string" && "itemQuantity zero") ||
          "itemQuantity value"
        }
      >
        {quantity}
      </span>
      <i
        className="fa-solid fa-circle-plus icon active-plus"
        style={{ color: "#fafafa" }}
        onClick={increament}
      ></i>
      <i
        className={
          (quantity > 0 && "fa-solid fa-circle-minus icon active-minus") ||
          "fa-solid fa-circle-minus icon deactive-minus"
        }
        style={{ color: "#fafafa" }}
        onClick={decreament}
      ></i>
      <i
        className="fa-solid fa-trash-can icon delete"
        style={{ color: "#fafafa" }}
        onClick={deleteList}
      ></i>
    </div>
  );
};

export default Item;
