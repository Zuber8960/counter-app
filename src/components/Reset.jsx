import React from "react";

const Reset = ({ products, prods, setProds, setArr }) => {
  function reset() {
    // console.log(prods.length);
    if (prods.length) {
      // const resetProducts = prods.map(({ ...product }) => {
      //   product.quantity = "Zero";
      //   return product;
      // });
      setProds(oldProducts => {
        return oldProducts.map(oldProduct => {
          oldProduct.quantity = "Zero";
          return oldProduct;
        })
      });
      setArr([]);
    }
  }

  function recycle() {
    if (prods.length === 0) {
      setArr([]);
      setProds(products);
      // return location.reload();
    }
  }

  return (
    <div className="reset-container">
      <i
        // className="fa-solid fa-arrows-rotate icon lightGreen"
        className={
          (prods.length && "fa-solid fa-arrows-rotate icon active-reset") ||
          "fa-solid fa-arrows-rotate icon deactive-reset"
        }
        style={{ color: "#fafafa" }}
        onClick={reset}
      ></i>
      <i
        // className="fa-solid fa-recycle icon lightBlue"
        className={
          (prods.length == 0 && "fa-solid fa-recycle icon active-recycle") ||
          "fa-solid fa-recycle icon deactive-recycle"
        }
        style={{ color: "#fafafa" }}
        onClick={recycle}
      ></i>
    </div>
  );
};

export default Reset;
